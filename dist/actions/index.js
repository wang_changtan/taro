"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asyncAdd = asyncAdd;
function asyncAdd(state, dispatch, params) {
  var _state$num = state.num,
      num = _state$num === undefined ? 0 : _state$num;

  setTimeout(function () {
    if (dispatch) {
      dispatch({
        type: "updateState",
        data: {
          num: num + 1
        }
      });
    }
  }, 2000);
}