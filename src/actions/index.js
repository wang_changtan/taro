
export function asyncAdd(state, dispatch, params) {
    let {num = 0} = state;
    setTimeout(()=>{
        if(dispatch){
            dispatch({
                type: "updateState",
                data: {
                    num: num + 1
                }
            })
        }
    },2000)
}