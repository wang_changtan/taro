import {LOGIN_TIMEOUT} from "../constants/login"
const INITIAL_STATE = {

};

export default function login (state = INITIAL_STATE, action) {
    switch (action.type) {
        case LOGIN_TIMEOUT:
            return {
                ...state,
                ...action.data,
            };
        default:
            return state
    }
}