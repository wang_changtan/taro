import {LOGIN_TIMEOUT} from "../constants/login";

const INITIAL_STATE = {
    loginTimeout: false,
    num: 0
};

export default function global (state = INITIAL_STATE, action) {
    switch (action.type) {
        case LOGIN_TIMEOUT:
            return {
                ...state,
                loginTimeout: action.data
            };
        case "updateState":
            return {
                ...state,
                ...action.data,
            }
        default:
            return state
    }
}