import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'


export default class Test extends Component {

    config = {
        navigationBarTitleText: '测试'
    }

    componentWillMount () { }

    componentDidMount () { }

    componentWillUnmount () { }

    componentDidShow () { }

    componentDidHide () { }

    render () {
        return (
            <View className='index'>
            <Text>测试</Text>
        </View>
    )
    }
}