import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.less'
import {connect} from "@tarojs/redux";
import {LOGIN_TIMEOUT} from "../../constants/login";
import {asyncAdd} from "../../actions";

@connect(({login, global})=>{
  console.log('>>>>',global,login);
  return {
    login,
    global
  }
})
export default class Index extends Component {

  config = {
    navigationBarTitleText: '首页'
  };

  constructor(props) {
    super(props);
  }

  componentWillMount () { }

  componentDidMount () {

  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }


  onClick(){
    let {dispatch, global} = this.props;
    // if(dispatch){
    //   dispatch({
    //     type:LOGIN_TIMEOUT,
    //     data: true
    //   })
    // }

    asyncAdd(global,dispatch,{})

  }

  render () {
    let {global: {num}} = this.props;
    return (
      <View className='index'>
        <Text type={"primary"} onClick={this.onClick.bind(this)}>异步请求数据</Text>
        {
          num && <View>{num}</View>
        }
      </View>
    )
  }
}
